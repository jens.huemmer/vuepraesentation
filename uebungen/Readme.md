# Übungsaufgaben
## 1. Übung - ToDo Liste
Sorgen Sie dafür, dass das Textfeld nach dem hinzufügen dieses Todos wieder geleert wird
### Aufgabe
https://jsfiddle.net/jhuemmer/6Lw9ntr4/
### Lösungsvorschlag
  https://jsfiddle.net/jhuemmer/az7vf4Lu/28/  

### Tipps
#### JavaScript
Zeile 17 
___
## 2. Übung - Zähler
Definieren Sie die Methode count, sodass der Zähler mit jedem Knopfdruck um 1 hochgezählt wird  
Fügen Sie noch einen weiteren Knopf hinzu, um den Zähler wieder um 1 herunter zu zählen
### Aufgabe
https://jsfiddle.net/jhuemmer/y2j05qwh/22/
### Lösungsvorschlag
https://jsfiddle.net/jhuemmer/2z8s4L7a/41/
### Tipps
#### JavaScript
Zeile 9 - 15
___
## 3. Übung - Timer
Formatieren Sie die Ausgabe der Zeit, sodass Sie besser lesbar wird  
`15` => `0:0:15`  
`90` => `0:1:30`  
`512` => `0:8:32`  
### Aufgabe  
https://jsfiddle.net/jhuemmer/1cdv9jok/21/
### Lösungsvorschlag
https://jsfiddle.net/jhuemmer/7syfz5Lr/139/
### Tipps
### HTML
  Zeile 18
#### JavaScript
  Zeile 18 - 19