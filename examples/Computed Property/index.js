const App = {
    computed: {
        //Text Anzeige
        text() {
            let output = 'Du musst noch folgende dinge erledingen: ';
            for(let todo of this.todos) {
                output = output + todo.text + ', ';
            }
            return output
        }
    },
    data: function() {
        return {
            newTodo: '', // Text der im Textfeld steht
            todos: [ // Liste aller Todos in der Tabelle
                { text: 'Hausaufgaben Machen' },
                { text: 'lernen' },
            ]
        }
    },
    methods: {
        // Methode, die aufgerufen wird, wenn jemand auf den Knopf "Todo hinzufügen" klickt
        addTodo: function() {
            this.todos.push({
                text: this.newTodo,
            })
        },
        // Methode, die aufgerufen wird, wenn ein Todo gelöscht werden soll
        deleteListItem: function(item) {
            var index = this.todos.map((e)=>e.text).indexOf(item);
            if (index !== -1) {
                this.todos.splice(index, 1);
            }
        },
    }
}
const app = Vue.createApp(App)
// my-list-item Component
app.component('my-list-item', {
    props: ['todo'],
    template: `
        <li>
            {{ todo.text }} 
            <button
                v-on:click="$emit('delete',todo.text)">
                Löschen
            </button>
        </li>
    `
})
app.mount('#app')