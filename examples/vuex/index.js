const App = {
    computed: {
        // alle Todos
        ...Vuex.mapState({
            todos: state => state.todos,
        }),
        // gefilterte Todos
        ...Vuex.mapGetters([
            'doneTodos',
            'countDoneTodos'
        ]),
        // Text
        text() {
            let output = 'Du musst noch folgende dinge erledingen: ';
            for(let todo of this.todos) {
                output = output + todo.text + ', ';
            }
            return output
        }
    },
    data: function() {
        return {
            newTodo: '', // neues Todo
        }
    },
    methods: {
        // Methode um neues todo hinzuzufügen
        addTodo: function() {
            this.todos.push({
                text: this.newTodo,
            })
        },
        // Methode um Todo zu löschen
        deleteListItem: function(item) {
            var index = this.todos.map((e)=>e.text).indexOf(item);
            if (index !== -1) {
                this.todos.splice(index, 1);
            }
        },
    }
}
const app = Vue.createApp(App)
// Vuex Store
const store = Vuex.createStore({
    state () {
        return {
            todos: [ // alle Todos
                {
                    text: 'Hausaufgaben Machen',
                    done: true,
                },
                {
                    text: 'lernen',
                    done: false,
                },
            ]
        }
    },
    getters: {
        // Fertige Todos
        doneTodos: (state) => {
            return state.todos.filter(todo=> todo.done);
        },
        // Anzahl der Fertigen Todos
        countDoneTodos: (state, getters) => {
            return getters.doneTodos.length
        }
    }
})
app.use(store)
// my-list-item Komponente
app.component('my-list-item', {
    props: ['todo'],
    template: `
        <li>
            {{ todo.text }} 
            <button
                v-on:click="$emit('delete',todo.text)">
                Löschen
            </button>
        </li>
    `
})
app.mount('#app')