const App = {
    data: function() {
        return {
            newTodo: '',
            todos: [
                { text: 'Hausaufgaben Machen' },
                { text: 'lernen' },
            ]
        }
    },
    methods: {
        //List-Item hinzufügen
        addTodo: function() {
            this.todos.push({
                text: this.newTodo,
            })
        },
        //List-Item Entfernen
        deleteListItem: function(item) {
            var index = this.todos.map((e)=>e.text).indexOf(item);
            if (index !== -1) {
                this.todos.splice(index, 1);
            }
        },
    }
}
const app = Vue.createApp(App)

//my-list-item Komponente
app.component('my-list-item', {
    props: ['todo'],
    template: `
        <li>
            {{ todo.text }} 
            <button
                v-on:click="$emit('delete',todo.text)">
                Löschen
            </button>
        </li>
    `
})
app.mount('#app')